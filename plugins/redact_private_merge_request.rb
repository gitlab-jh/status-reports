
require 'forwardable'

module RedactPrivateMergeRequest
  extend Forwardable

  def_delegators :display_strategy,
    :display_author,
    :display_title,
    :display_labels,
    :display_created_days_ago,
    :display_merged_days_ago

  private

  def visibility
    @visibility ||= request_project(resource[:project_id])[:visibility]
  end

  def display_strategy
    @display_strategy ||= if visibility == 'public'
      Revealed.new(self)
    else
      Redacted.new(self)
    end
  end

  DisplayStrategy = Struct.new(:resource) do
    def display_author
      raise NotImplementedError
    end

    def display_title
      raise NotImplementedError
    end

    def display_labels
      raise NotImplementedError
    end

    def display_created_days_ago
      raise NotImplementedError
    end

    def display_merged_days_ago
      raise NotImplementedError
    end
  end

  class Revealed < DisplayStrategy
    def display_author
      escaped_name = CGI.escape_html(resource.author)

      "`@#{escaped_name}` - "
    end

    def display_title
      CGI.escape_html(resource.resource[:title])
    end

    def display_labels
      resource.labels.map do |label|
        escaped_name = CGI.escape_html(label.name)

        %Q{~"#{escaped_name}"}
      end.join(', ')
    end

    def display_created_days_ago
      days = (Date.today - Date.parse(resource.resource[:created_at])).to_i

      "#{days} days old"
    end

    def display_merged_days_ago
      days = (Date.today - Date.parse(resource.resource[:merged_at] || '1970-01-01')).to_i

      "merged #{days} days ago"
    end
  end

  class Redacted < DisplayStrategy
    def display_author
      ''
    end

    def display_title
      'confidential'
    end

    def display_labels
      ''
    end

    def display_created_days_ago
      ''
    end

    def display_merged_days_ago
      ''
    end
  end
end

Gitlab::Triage::Resource::Context.prepend RedactPrivateMergeRequest

