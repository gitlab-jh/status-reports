# Weekly status reports

This project contains the following:

* Weekly generated reports in issues
* Scripts to generate the reports
* [Scheduled pipeline](https://gitlab.com/gitlab-jh/status-reports/-/pipeline_schedules/105170/edit) to run the scripts
